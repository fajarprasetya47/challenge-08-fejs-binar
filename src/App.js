import logo from './logo.svg';
import './App.css';
import ListCar from "./ListCar/ListCar";
import Dropzone from "react-dropzone-uploader";
import "react-dropzone-uploader/dist/styles.css";
import { useState, useEffect } from "react";
import { Bar } from 'react-chartjs-2'
//eslint-disable-next-line
import { Chart } from 'chart.js/auto'

function App() {
  const [files, setFiles] = useState();
  const [dataCar, setDataCar] = useState([]);

  const getDataCar = async () => {
    const data = await fetch('https://rent-cars-api.herokuapp.com/admin/car');
    const result = await data.json();
    setDataCar(result);
    //console.log(data)
  }

  const getUploadParams = ({ meta }) => {
    setFiles(meta);
    return { url: "https://httpbin.org/post" };
  };

  const hanldeChangeStatus = ({ meta, file }, status) => {
    console.log(`Perubahaan terjadi ${status}`);
    setFiles(meta);
  };

  const handleSubmit = (files, allFiles) => {
    console.log(`Files => ${files} && all Files => ${allFiles}`);
    allFiles.forEach((data) => data.remove());
  };

  useEffect(() => {
    getDataCar();
  }, [files]);

  const dataChart = {
    labels: ['January', 'February', 'March'],
    datasets: [
      {
        label: 'Terjual',
        backgroundColor: 'red',
        data: [45, 32, 10],
      },
      {
        label: 'Tidak Terjual',
        backgroundColor: 'purple',
        data: [5, 9, 40],
      },
    ]
  }

  const dataChartApi = {
    labels: dataCar?.map((item) => item.name),
    datasets: [
      {
        label: 'Harga',
        backgroundColor: 'orange',
        data: dataCar?.map((item) => item.price),
      },
    ]
  }

  return (
    <>
      {/* data visualisasi react chart js */}
      <h1>WELCOME!</h1>
      <div style={{ width: '800px' }}>
        <Bar data={dataChart} redraw={true} />
        <Bar data={dataChartApi} redraw={true} />
      </div>

      {/* media handling with dropzone */}
      <Dropzone
        getUploadParams={getUploadParams}
        onChangeStatus={hanldeChangeStatus}
        onSubmit={handleSubmit}
        accept="image/*"
      />
      <ListCar />
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    </>
  );
}

export default App;