//penjumlahan
const penjumlahan = (a,b) => {
    return a+b;
}
const pengurangan = (a,b) => {
    return a-b;
}
const perkalian = (a,b) => {
    return a*b;
}
const pembagian = (a,b) => {
    return a/b;
}
const rataan = (a,b) => {
    return (a+b)/2;
}


test('should be sum', ()=>{
     expect(penjumlahan(1,1)).toBe(2)
});
test('should be minus', ()=>{
     expect(pengurangan(1,1)).toBe(0)
});
test('should be multiple', ()=>{
     expect(perkalian(1,1)).toBe(1)
});
test('should be divided', ()=>{
     expect(pembagian(1,1)).toBe(1)
});
test('should be average', ()=>{
     expect(rataan(3,1)).toBe(2)
     expect(rataan(3,3)).toBe(3)
});